jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        //allSliders====================================================================================================
        if ($.exists(".sliderLeft")) {
            $('.sliderLeft').bxSlider({
                maxSlides: 2,
                moveSlides: 1,
                pager: false,
                slideWidth: 270,
                slideMargin: 30,
                nextSelector: '#sliderLeft-next',
                prevSelector: '#sliderLeft-prev',
                nextText: '',
                prevText: ''
            });
        }

        if ($.exists(".sliderRight")) {
            $('.sliderRight').bxSlider({
                maxSlides: 2,
                moveSlides: 1,
                pager: false,
                slideWidth: 270,
                slideMargin: 30,
                nextSelector: '#sliderRight-next',
                prevSelector: '#sliderRight-prev',
                nextText: '',
                prevText: ''
            });
        }

        if ($.exists(".indexSlider")) {
            $('.indexSlider').bxSlider({
                moveSlides: 1,
                pager: true,
                nextText: '',
                prevText: ''
            });
        }

        if ($.exists(".indexInsideSlider")) {
            $('.indexInsideSlider').bxSlider({
                moveSlides: 1,
                pager: false,
                nextSelector: '#indexSliderItem-next',
                prevSelector: '#indexSliderItem-prev',
                nextText: '',
                prevText: ''
            });
        }

        if ($.exists(".productsSlider")) {
            $('.productsSlider').bxSlider({
                maxSlides: 4,
                moveSlides: 1,
                pager: false,
                slideWidth: 292,
                slideMargin: 0,
                nextSelector: '#productsBox-next',
                prevSelector: '#productsBox-prev',
                nextText: '',
                prevText: ''
            });
        }

        if($.exists(".productDescSlider")) {
            $('.productDescSlider').lightSlider({
                gallery:true,
                item:1,
                loop:true,
                thumbItem:4,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition:'left',
                onSliderLoad: function() {
                    $('.productDescSlider').removeClass('cS-hidden');
                }
            });
        }

        if ($.exists(".productDescSliderBig")) {
            $('.productDescSliderBig').bxSlider({
                moveSlides: 1,
                pager: false,
                nextSelector: '#productDescSliderBig-next',
                prevSelector: '#productDescSliderBig-prev',
                nextText: '',
                prevText: ''
            });
        }
        //====================================================================================================allSliders








        if ($.exists("#blogItems")) {
            var $container = $('#blogItems');
            $container.packery({
                itemSelector: '.blogItem',
                gutter: 30
            });
        }

        if ($.exists(".accordion")) {
            var allhideAcc = $('.accordion .hideAcc').hide();
            var alltitleAcc = $('.accordion .titleAcc');

            $('.accordion .titleAcc').click(function (e) {
                e.preventDefault();

                if ($(this).next().hasClass('active')) {
                    $(this).next().removeClass('active').stop().slideUp();
                    $(this).removeClass('active');
                } else {
                    allhideAcc.removeClass('active').stop().slideUp();
                    alltitleAcc.removeClass('active');
                    $(this).next().addClass('active').stop().slideDown();
                    $(this).addClass('active');
                }
            });

        }

        if($.exists(".openHidden")) {
            $('.openHidden').click(function (e) {
                e.preventDefault();
                $(e.target).next('.hiddenBox').stop().slideToggle("fast");
            });
        }

        if($.exists(".hideItem")) {
            $('.hideItem').click(function (e) {
                e.preventDefault();
                $(e.target).parent().slideUp("fast");
            });
        }

        if ($.exists(".productBoxContainer")) {
            var $container2 = $('.productBoxContainer');
            $container2.packery({
                itemSelector: '.productBoxItem',
                gutter: 0
            });
        }

        if($.exists(".open-popup")) {
            $('.open-popup').click(function (e) {
                e.preventDefault();
                var href = $(this).attr('href');

                if ($(href).hasClass('active-popup')) {
                    $(href).removeClass('active-popup');
                    $("body").removeClass('active');
                }else{
                    $(href).addClass('active-popup');
                    $("body").addClass('active');
                }
            });
        }

        if($.exists(".serviceInfo")) {
            $( document ).tooltip({
                track: true
            });
        }

        if($.exists(".numberBox")) {
            $('.numberP').on('click', function(e){
                e.preventDefault();
                var numbersVal = $(e.target).parent().children(".numberText").val();
                var result = parseInt(numbersVal) + 1;
                $(e.target).parent().children(".numberText").val(result);
            });
            $(".numberM").on('click', function(e){
                e.preventDefault();
                var numbersVal = $(e.target).parent().children(".numberText").val();
                var result = parseInt(numbersVal) - 1;
                $(e.target).parent().children(".numberText").val(result);

                if($(e.target).parent().children(".numberText").val() <= 1){
                    $(e.target).parent().children(".numberText").val(1);
                }
            });

            $('.numberP').on('tap', function(e){
                e.preventDefault();
                var numbersVal = $(e.target).parent().children(".numberText").val();
                var result = parseInt(numbersVal) + 1;
                $(e.target).parent().children(".numberText").val(result);
            });
            $(".numberM").on('tap', function(e){
                e.preventDefault();
                var numbersVal = $(e.target).parent().children(".numberText").val();
                var result = parseInt(numbersVal) - 1;
                $(e.target).parent().children(".numberText").val(result);

                if($(e.target).parent().children(".numberText").val() <= 1){
                    $(e.target).parent().children(".numberText").val(1);
                }
            });
        }

        if($.exists(".selectBox")) {
            $( ".selectBox" ).selectmenu();
        }

        if($.exists(".scrollbar-vista")) {
            $('.scrollbar-vista').scrollbar({
                "showArrows": true,
                "scrollx": "advanced",
                "scrolly": "advanced"
            });
        }




    });
}).call(this);


